// RobotBuilder Version: 2.0
//
// This file was generated by RobotBuilder. It contains sections of
// code that are automatically generated and assigned by robotbuilder.
// These sections will be updated in the future when you export to
// Java from RobotBuilder. Do not put any code or make any change in
// the blocks indicating autogenerated code or it will be lost on an
// update. Deleting the comments indicating the section will prevent
// it from being updated in the future.


package org.usfirst.frc3777.BeanCityBots2018.commands;
import edu.wpi.first.wpilibj.command.Command;

import java.util.Timer;
import java.util.TimerTask;

import org.usfirst.frc3777.BeanCityBots2018.Robot;

/**
 * TODO This help message doesn't work when hovering over this 'class' in the program.
 * (djh) This command is the primary autonomous command this year.
 * @param boolean This tells whether the Flopper should shoot at the end of autonomous.
 * @return nothing
 */
public class cmdAutoDrvStrghtTimed extends Command {

    // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=VARIABLE_DECLARATIONS
	private boolean m_Flopp;
 
    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=VARIABLE_DECLARATIONS
	
	//djh The SpeedCorrection factor will be different for each robot.
	//djh	Factors include differences in motors, wiring, friction, gears, lubrication, etc.
	//djh	Presently the way the code is written, the SpeedCorrection is only for forward!!
	private final double SPEED_LEFT = -0.5;				//djh
	private final double SPEED_RIGHT = SPEED_LEFT;		//djh
	private final double SPEED_RIGHT_CORRECTION = 1.0;	//djh Used to compensate righ/left drive train.
	private final double SPEED_ZERO = 0.0;				//djh
	
	private final long DRV_DURATION = 5000;			//djh Drive duration in milliseconds.
	
	private boolean done = false;				//djh Used to trigger isFinished() --> end()

	//djh ---------------------------------------------------------------------
    // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=CONSTRUCTOR
    public cmdAutoDrvStrghtTimed(boolean flopp) {

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=CONSTRUCTOR
        // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=VARIABLE_SETTING
    	m_Flopp = flopp;

        // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=VARIABLE_SETTING
        // BEGIN AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=REQUIRES
        requires(Robot.subsDrive);

    // END AUTOGENERATED CODE, SOURCE=ROBOTBUILDER ID=REQUIRES
    }

    //djh ---------------------------------------------------------------------
    // Called just before this Command runs the first time
    @Override
    protected void initialize() {
    	//djh Setup 'interrupt()' to be finished after specified duration of time.
    	//djh (see constructor below, for CommandTimerTask().
    	CommandTimerTask interrupt = new CommandTimerTask();	//djh
		new Timer().schedule(interrupt, DRV_DURATION);		//djh interrupt is run after delay.
    	done = false;									//djh
    }

    //djh ---------------------------------------------------------------------
    // Called repeatedly when this Command is scheduled to run
    @Override
    protected void execute() {
    	Robot.subsDrive.zshiftForward();
    	Robot.subsDrive.periodic( SPEED_LEFT , SPEED_RIGHT*SPEED_RIGHT_CORRECTION );
    }

    //djh ---------------------------------------------------------------------
    // Make this return true when this Command no longer needs to run execute()
    //djh	When it does return 'true', then the Scheduler will call end().
    @Override
    protected boolean isFinished() {
        // return false;				//djh "return false" keeps it running,
    									//djh	until interrupted by another command
    									//djh	that 'requires' the same subsystem(s).
        return done;					//djh When 'done' changes to true, --> end().
    }

    //djh ---------------------------------------------------------------------
    // Called once after isFinished returns true
    @Override
    protected void end() {
    	//djh TODO change this to use a 'shift to neutral' method() on the drive subsystem??
    	Robot.subsDrive.periodic(SPEED_ZERO,SPEED_ZERO);	//djh: set speed to zero when finished driving.
    	
    	//djh This would be the place to decide to Flop/Dump the game cube??
    	if(m_Flopp == true){
    		new cmdFlopperDump().start();
    	}
    }

    //djh ---------------------------------------------------------------------
    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    @Override
    protected void interrupted() {
    }
    
    
    //djh ---------------------------------------------------------------------
    //djh Added this CommandTimerTask, from an example found...somewhere...
    //djh This requires java.util TimerTask
//djh    private class CommandTimerTask extends TimerTask {
    //djh this requires Timer form java.util, or ??? from: edu.wpi.first.wpilib
    private class CommandTimerTask extends TimerTask {
    	public void run(){
    		done = true;
    	}
    }
    
}
